const fs = require('fs')
const https = require('https')
const moment = require('moment')
const Twitter = require('twitter')
const mysql = require('mysql')
const twitterSecrets = JSON.parse(fs.readFileSync('.twitter-secrets.json'))
const twitterClient = new Twitter(twitterSecrets)

const fetch = (url, base64) => {
  return new Promise((resolve, reject) => {
    https.get(url, (res) => {
      if (res.statusCode !== 200) {
        return reject(new Error(`${url} returned unexpected status code: ${res.statusCode}`))
      }

      let data = []
      res.on('data', (chunk) => {
        data.push(chunk)
      })

      res.on('end', () => {
        if (base64) {
          const buffer = Buffer.concat(data)
          resolve(buffer.toString('base64'))
        } else {
          resolve(data.join(''))
        }
      })
    }).on('error', reject)
  })
}

const sendTweet = ({ text, mediaIdString }) => {
  twitterClient.post('statuses/update', { status: text, media_ids: mediaIdString }, (error, tweet) => {
    if (error) throw new Error(JSON.stringify(error))
    console.log(`https://twitter.com/${tweet.user.screen_name}/status/${tweet.id_str}`)
  })
}

const uploadImage = (base64ImageData) => {
  return twitterClient.post('media/upload', { media_data: base64ImageData })
    .then((media) => media.media_id_string)
}

const mysqlSecrets = JSON.parse(fs.readFileSync('.mysql-secrets.json'))
const mysqlConnection = mysql.createConnection(mysqlSecrets)

if (!process.env.DEBUGGING) {
  mysqlConnection.connect()
}

const createTweet = (post) => {
  const postDate = moment(post.post_date)
  const daysSinceComic = moment().diff(postDate, 'days')
  const comicUrl = `https://fonflatter.de/${postDate.format('YYYY/MM/DD')}/${post.post_name}/?TBT`
  const text = `Heute vor ${daysSinceComic} Tagen erschien dieser fetzige Comic. #TBT\n${comicUrl}`
  const imageUrl = `https://fonflatter.de/${postDate.year()}/fred_${postDate.format('YYYY-MM-DD')}.png`
  return fetch(imageUrl, true)
    .then(uploadImage)
    .then(mediaIdString => ({ text, mediaIdString }))
}

if (process.env.DEBUGGING) {
  createTweet({ post_date: '2008-11-28 00:01:00', post_name: '1166-was-du-denkst' })
    .then(sendTweet)
} else {
  mysqlConnection.query(`
    SELECT post_date, post_name
    FROM ff_postmeta
    INNER JOIN ff_posts
    ON ff_posts.ID = post_id
    WHERE meta_key = 'tbt'
    AND meta_value = "${moment().format('Y-m-d')}"
    LIMIT 1
  `, (error, results) => {
    if (error) throw error
    const [post] = results
    createTweet(post)
      .then(sendTweet)
    mysqlConnection.end()
  })
}
